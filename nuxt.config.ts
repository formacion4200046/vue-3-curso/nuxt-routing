// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: false },
  nitro:{
    prerender:{
      routes:['/services','/contact','/about','/']
    },
    plugins:['~/server/index.ts']
  },
  runtimeConfig:{
    mongodbUri:process.env.MONGODB_URI
  }
})
