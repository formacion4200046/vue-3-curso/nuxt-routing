export default class NoteService {
    private base_uri='https://localhost:3000/api/notes/'

    public async getNote(){
        const uri=this.base_uri
        const rawResponse=await fetch(uri,{
            method:'GET'
        })

        const response=await rawResponse.json()
        console.log(response);

        return response
    }
    
}